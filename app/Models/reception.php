<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;


class reception extends Authenticatable
{
    use HasFactory;

    public function fees()
    {
        return $this->hasOne(fees::class);
    }

    public function patients()
    {
        return $this->hasMany(patients::class);
    }

    public function rooms()
    {
        return $this->hasMany(rooms::class);
    }
 public function clinic()
 {
     return $this->belongsTo(clinic::class);
 }

 public function specalities()
 {
     return $this->hasMany(specalities::class);
 }
}
