<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class city extends Model
{
    use HasFactory;

    public function patients()
    {
        return $this->hasMany(patients::class);
    }
    public function doctors()
    {
        return $this->hasMany(doctors::class);
    }

    public function clinic()
    {
        return $this->hasMany(clinic::class);
    }

    public function employees()
    {
        return $this->hasMany(employee::class);
    }
}
