<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    use HasFactory;
    public function clinic()
    {
        return $this->belongsTo(clinic::class);
    }

    public function cities()
    {
        return $this->belongsTo(city::class);
    }
}
