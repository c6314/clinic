<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class doctor extends Model
{
    use HasFactory;
    public function room()
    {
        return $this->hasOne(room::class);
    }

    public function clinic()
    {
        return $this->belongsTo(clinic::class);
    }

    public function specalities()
    {
        return $this->belongsTo(specalities::class);
    }

    public function city()
    {
        return $this->belongsTo(city::class);
    }

public function treatments()
{
    return $this->hasMany(treatment::class);
}


private function patients()
{
    return $this->belongsToMany(patients::class,'doctor_has_Patients');
}
}
