<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fees extends Model
{
    use HasFactory;

    public function reception()
    {
        return $this->belongsTo(reception::class);
    }

    public function Patients()
    {
        return $this->belongsTo(Patients::class);
    }
}
