<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patients extends Model
{
    use HasFactory;

    public function fees()
    {
        return $this->hasOne(fees::class);
    }

    public function detail()
    {
        return $this->hasOne(details::class);
    }

    public function room()
    {
        return $this->belongsTo(room::class);
    }

    public function reception()
    {
        return $this->belongsTo(reception::class);
    }

    public function city()
    {
        return $this->belongsTo(city::class);
    }

    public function clinic()
    {
        return $this->belongsTo(clinic::class);
    }

    public function treatments()
{
    return $this->hasMany(treatment::class);
}
private function doctors()
{
    return $this->belongsToMany(doctors::class ,'doctor_has_Patients');
}

private function specalities()
{
return $this->belongsToMany(specalities::class ,'Patients_has_specalities');
}
}
