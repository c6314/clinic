<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class clinic extends Model
{
    use HasFactory;
    public function patients()
    {
        return $this->hasMany(patients::class);
    }

    public function rooms()
    {
        return $this->hasMany(rooms::class);
    }

    public function doctors()
    {
        return $this->hasMany(doctor::class);
    }

    public function equipments()
    {
        return $this->hasMany(equipments::class);
    }

    public function reception()
    {
        return $this->hasOne(reception::class);
    }

    public function city()
    {
        return $this->belongsTo(city::class);
    }

    public function employees()
    {
        return $this->hasMany(employee::class);
    }
    
}
