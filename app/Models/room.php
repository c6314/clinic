<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class room extends Model
{
    use HasFactory;
    public function patiants()
    {
        return $this->hasMany(patiants::class);
    }

    public function reception()
    {
        return $this->belongsTo(reception::class);
    }

    public function equipments()
    {
        return $this->hasMany(equipments::class);
    }

    public function clinic()
    {
        return $this->belongsTo(clinic::class);
    }

    public function doctor()
    {
        return $this->belongsTo(doctor::class);
    }
}


