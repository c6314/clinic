<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class specalities extends Model
{
    use HasFactory;
    public function doctors()
    {
        return $this->hasMany(doctor::class);
    }
    public function clinic()
    {
        return $this->belongsTo(clinic::class);
    }


   
    private function patients()
    {
        return $this->belongsToMany(patients::class ,'Patients_has_specalities');
    }
}
