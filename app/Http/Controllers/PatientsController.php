<?php

namespace App\Http\Controllers;

use App\Models\Patients;
use App\Http\Requests\StorePatientsRequest;
use App\Http\Requests\UpdatePatientsRequest;
use App\Models\city;
use App\Models\clinic;
use App\Models\doctor;
use App\Models\fees;
use App\Models\reception;
use App\Models\room;
use App\Models\specalities;
use Illuminate\Http\Request;

class PatientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *

     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patients::all();
        $doctors = doctor::all();
        return response()->view('cms.patient.index' , compact('patients','doctors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // $roles = specalities::with('specalities')->with('clinic')->with('room')->get();
        $specalities = specalities::all();
        $clinics = clinic::all();
        $rooms = room::all();
        $doctors = doctor::all();
        $patients = Patients::all();
        $receptions=reception::all();
        $fees=fees::all();
        $cities=city::all();

        return response()->view('cms.patient.create' ,
        compact('specalities','clinics','rooms','doctors','receptions','fees','cities'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorepatientRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatior = Validator($request->all(), [
            'name' => 'required|string|min:3|max:20',
            'age' => 'required|string',
            'phone' => 'required|string|min:3|max:20',
            'address' => 'required|string|min:5|max:20',

        ]);

        if (!$validatior->fails()) {
            $patients = new Patients();
            $patients->doctor_id = $request->get('doctor_id');
            $patients->clinic_id = $request->get('clinic_id');
            $patients->room_id = $request->get('room_id');
            $patients->reception_id = $request->get('reception_id');
            $patients->fees_id = $request->get('fees_id');
            $patients->city_id = $request->get('city_id');
            $patients->name = $request->get('name');
            $patients->age = $request->get('age');
            $patients->phone = $request->get('phone');
            $patients->address = $request->get('address');
            $patients->gender = $request->get('gender');

            $isSaved = $patients->save();
            if ($isSaved) {
                return response()->json(['icon' => 'success', 'title' => 'saved is Successfully'], $isSaved ? 201 : 400);
            } else {
                return response()->json(['icon' => 'error', 'title' => $validatior->getMessageBag()->first()], 400);
            }
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function showshow($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patients = patients::findOrFail($id);


        // $patients = Patients::all();
        $clinics = clinic::all();
        $rooms = room::all();
        $doctors = doctor::all();
        $patient = Patients::all();
        $receptions=reception::all();
        $fees=fees::all();
        $cities=city::all();

        return response()->view('cms.patient.edit' ,
        compact('patients','clinics','rooms','doctors','receptions','fees','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatepatientRequest  $request
     * @param  \App\Models\patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
    $validatior = Validator($request->all(),[
        'name' => 'required|string|min:3|max:20',
        'age' => 'required|string',
        'phone' => 'required|string|min:3|max:20',
        'address' => 'required|string|min:5|max:20',

    ]);

    if(!$validatior->fails()){
        $patients = patients::findOrFail($id);

        $patients->name = $request->get('name');
        $patients->age = $request->get('age');
        $patients->address = $request->get('address');
        $patients->doctor_id = $request->get('doctor_id');
        $patients->clinic_id = $request->get('clinic_id');
        $patients->room_id = $request->get('room_id');
        $patients->reception_id = $request->get('reception_id');
        $patients->fees_id = $request->get('fees_id');
        $patients->city_id = $request->get('city_id');

        $patients->phone = $request->get('phone');
        $patients->gender = $request->get('gender');

        $isSaved = $patients->save();
        return response()->json(['icon' => 'success', 'title' => 'update is Successfully'], $isSaved ? 201 : 400);
    } else {

        return response()->json(['icon' => 'error', 'title' => $validatior->getMessageBag()->first()], 400);
    }

}


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patients = Patients::destroy($id);
        return response()->json(['icon' => 'success', 'title' => 'Deleted is Successfully'], $patients ? 200 : 400);

    }
}
