<?php

namespace App\Http\Controllers;

use App\Models\employee;
use App\Http\Requests\StoreemployeeRequest;
use App\Http\Requests\UpdateemployeeRequest;
use App\Models\city;
use App\Models\clinic;
// use App\Models\fees;
use App\Models\fees;
use App\Models\cities;
// use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;

class employeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $employees = employee::all();
        $employees = employee::with('cities')->get();
        return response()->view('cms.employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $roles = cities::with('cities')->with('clinic')->with('fees')->get();
        $cities = city::all();
        $clinics = clinic::all();
        $fees = fees::all();


        return response()->view('cms.employee.create', compact('cities', 'clinics', 'fees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreemployeeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // name			email	salary	cv	image	city_id	clinic_id	fees_id	created_at	updated_at

        $validatior = Validator($request->all(), [
            'name' => 'required|string|min:3|max:20',
            // 'email' => 'required|email',
            'phone' => 'required|string|min:5|max:20',
            'address' => 'required|string|min:5|max:20',
            // 'salary' => 'required|string|min:5|max:20',
            'cv' => 'required',
            'image' => 'required',


        ]);

        if (!$validatior->fails()) {
            $employees = new employee();
            $employees->city_id = $request->get('city_id');
            $employees->clinic_id = $request->get('clinic_id');
            $employees->fees_id = $request->get('fees_id');
            $employees->name = $request->get('name');
            $employees->phone = $request->get('phone');
            $employees->address = $request->get('address');

            $image = $request->file('image');
            $imageName = time() . 'image.' . $image->getClientOriginalExtension();
            $image->move('images/employee', $imageName);
            $employees->image = $imageName;


            if (request()->hasFile('cv')) {
                $cv = $request->file('cv');
                $fileName = time() . 'cv.' . $cv->getClientOriginalExtension();
                $cv->move('file/employee', $fileName);
                $employees->cv = $fileName;
            }
            $isSaved = $employees->save();
            if ($isSaved) {
                return response()->json(['icon' => 'success', 'title' => 'saved is Successfully'], $isSaved ? 201 : 400);
            } else {
                return response()->json(['icon' => 'error', 'title' => $validatior->getMessageBag()->first()], 400);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function showshow($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employees = employee::findOrFail($id);
        $cities = city::all();
        $clinics = clinic::all();
        $fees = fees::all();


        return response()->view('cms.employee.edit', compact('cities', 'clinics', 'employees', 'fees'));
        // return response()->view('cms.employee.edit' , compact('employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateemployeeRequest  $request
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatior = Validator($request->all(), [
            'name' => 'required|string|min:3|max:20',
            'phone' => 'required|string|min:5|max:20',
            'address' => 'required|string|min:5|max:20',
            'salary' => 'required|string|min:5|max:20',
            'cv' => 'required',
            'image' => 'required',
        ]);

        if (!$validatior->fails()) {
            $employees = employee::findOrFail($id);
            $employees->city_id = $request->get('city_id');
            $employees->clinic_id = $request->get('clinic_id');
            $employees->fees_id = $request->get('fees_id');
            $employees->name = $request->get('name');
            $employees->phone = $request->get('phone');
            $employees->address = $request->get('address');

            if (request()->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time() . 'image.' . $image->getClientOriginalExtension();
                $image->move('images/employee', $imageName);
                $employees->image = $imageName;
            }
            if (request()->hasFile('cv')) {
                $cv= $request->file('cv');
                $fileName = time() . 'cv.' . $cv->getClientOriginalExtension();
                $cv->move('file/employee', $fileName);
                $employees->cv= $fileName;
            }
            $isSaved = $employees->save();
            if ($isSaved) {
                return response()->json(['icon' => 'success', 'title' => 'Updated Successfully'], $isSaved ? 201 : 400);
             } else {
                return response()->json(['icon' => 'error', 'title' => $validatior->getMessageBag()->first()], 400);
            }
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employees = employee::destroy($id);
        return response()->json(['icon' => 'success', 'title' => 'Deleted Successfully'], $employees ? 201 : 400);

    }
}
