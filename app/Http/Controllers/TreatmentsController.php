<?php

namespace App\Http\Controllers;

use App\Models\treatments;
use App\Http\Requests\StoretreatmentsRequest;
use App\Http\Requests\UpdatetreatmentsRequest;
use App\Models\doctor;
use App\Models\Patients;
use  Illuminate\Http\Request;

class TreatmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $treatments = treatments::orderBy('id' , 'desc')->get();
        return response()->view('cms.treatments.index' , compact('treatments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $doctors= doctor::with('treatments')->get();
        $patients= Patients::with('treatments')->get();
        return response()->view('cms.treatments.create',compact('doctors','patients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator($request->all(), [
            'name'=>"required|min:3|max:25",

        ]);

        if (!$validator->fails()) {
            $treatments =new treatments();
            $treatments->name = $request->get('name');


            $isSaved = $treatments->save();

            return response()->json(['icon' => 'success', 'title' => 'saved is Successfully'], $isSaved ? 201 : 400);
        } else {

            return response()->json(['icon' => 'error', 'title' => $validator->getMessageBag()->first()], 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $treatments = treatments::findOrFail($id);
        return response()->view('cms.treatments.edit' , compact('treatments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator($request->all(), [
            'name'=>"required|min:3|max:25",

        ]);

        if (!$validator->fails()) {
            $treatments =treatments::findOrFail();
            $treatments->name = $request->get('name');
            $isSaved = $treatments->save();
            return response()->json(['icon' => 'success', 'title' => 'update is Successfully'], $isSaved ? 201 : 400);
        } else {

            return response()->json(['icon' => 'error', 'title' => $validator->getMessageBag()->first()], 400);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $treatments = treatments::destroy($id);
        return response()->json(['message' => $treatments ? "Deleted is Successfully" : "Deleted is Fieald"] ,  $treatments ? 200 : 400);
    }
}
