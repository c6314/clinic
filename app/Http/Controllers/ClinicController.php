<?php

namespace App\Http\Controllers;

use App\Models\clinic;
use App\Http\Requests\StoreclinicRequest;
use App\Http\Requests\UpdateclinicRequest;
use App\Models\city;
use App\Models\reception;
// use Illuminate\Http\Client\Request;
use App\Schedules;
use  Illuminate\Http\Request;

class ClinicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clinics = Clinic::orderBy('id' , 'desc')->get();
        return response()->view('cms.Clinic.index' , compact('clinics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities= city::with('clinic')->get();
        $receptions= reception::with('clinic')->get();
        return response()->view('cms.Clinic.create',compact('cities','receptions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator($request->all(), [
            'name'=>"required|min:3|max:25",
            'phone'=>"required|min:3|max:25",
        ]);

        if (!$validator->fails()) {
            $clinics =new Clinic();
            $clinics->name = $request->get('name');
            $clinics->phone = $request->get('phone');

            $isSaved = $clinics->save();

            return response()->json(['icon' => 'success', 'title' => 'saved is Successfully'], $isSaved ? 201 : 400);
        } else {

            return response()->json(['icon' => 'error', 'title' => $validator->getMessageBag()->first()], 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clinics = Clinic::findOrFail($id);
        return response()->view('cms.Clinic.edit' , compact('clinics'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator($request->all(), [
            'name'=>"required|min:3|max:25",
            'phone'=>"required|min:3|max:25",


        ]);
        if(!$validator->fails()){
            $clinics = clinic::findOrFail($id);
                $clinics->name = $request->get('name');
                $clinics->phone = $request->get('phone');
                // $clinics->reception_id = $request->get('reception_id');


            $isSaved = $clinics->save();
            return response()->json(['icon' => 'success', 'title' => 'update is Successfully'], $isSaved ? 201 : 400);
        } else {

            return response()->json(['icon' => 'error', 'title' => $validator->getMessageBag()->first()], 400);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clinics = Clinic::destroy($id);
        return response()->json(['message' => $clinics ? "Deleted Successfully" : "Delete Failed"] , $clinics ? 200 :400);
    }
}
