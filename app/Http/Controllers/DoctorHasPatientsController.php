<?php

namespace App\Http\Controllers;

use App\Models\Doctor_has_Patients;
use App\Http\Requests\StoreDoctor_has_PatientsRequest;
use App\Http\Requests\UpdateDoctor_has_PatientsRequest;

class DoctorHasPatientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDoctor_has_PatientsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDoctor_has_PatientsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Doctor_has_Patients  $doctor_has_Patients
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor_has_Patients $doctor_has_Patients)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Doctor_has_Patients  $doctor_has_Patients
     * @return \Illuminate\Http\Response
     */
    public function edit(Doctor_has_Patients $doctor_has_Patients)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDoctor_has_PatientsRequest  $request
     * @param  \App\Models\Doctor_has_Patients  $doctor_has_Patients
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDoctor_has_PatientsRequest $request, Doctor_has_Patients $doctor_has_Patients)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Doctor_has_Patients  $doctor_has_Patients
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor_has_Patients $doctor_has_Patients)
    {
        //
    }
}
