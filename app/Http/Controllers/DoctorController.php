<?php

namespace App\Http\Controllers;

use App\Models\doctor;
use App\Http\Requests\StoredoctorRequest;
use App\Http\Requests\UpdatedoctorRequest;
use App\Models\clinic;
use App\Models\room;
use App\Models\specalities;
// use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $doctors = doctor::all();
        $doctors = doctor::with('specalities')->get();
        return response()->view('cms.doctor.index' , compact('doctors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $roles = specalities::with('specalities')->with('clinic')->with('room')->get();
        $specalities = specalities::all();
        $clinics = clinic::all();
        $rooms = room::all();


        return response()->view('cms.doctor.create' ,compact('specalities','clinics','rooms'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoredoctorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validatior = Validator($request->all(),[
            'name' => 'required|string|min:3|max:20',
            'email' => 'required|email',
            'phone' => 'required|string|min:5|max:20',
            'address' => 'required|string|min:5|max:20',
            'salary' => 'required|string|min:5|max:20',
            'cv' => 'required',
            'image' => 'required',


        ]);

        if(!$validatior->fails()){
            $doctors = new doctor();
            $doctors->specalities_id = $request->get('specalities_id');
            $doctors->clinic_id = $request->get('clinic_id');
            $doctors->room_id = $request->get('room_id');
            $doctors->name = $request->get('name');
            $doctors->email = $request->get('email');
            $doctors->phone = $request->get('phone');
            $doctors->address = $request->get('address');
            $doctors->salary = $request->get('salary');

            $image = $request->file('image');;
            $imageName = time() . 'image.' . $image->getClientOriginalExtension();
            $image->move('images/doctor', $imageName);
            $doctors->image = $imageName;


            if (request()->hasFile('cv')) {
                $cv= $request->file('cv');;
                $fileName = time() . 'cv.' . $cv->getClientOriginalExtension();
                $cv->move('file/doctor', $fileName);
                $doctors->cv= $fileName;
            }

            $isSaved = $doctors->save();
            return response()->json(['icon' => 'success', 'title' => 'Saved is Successfully'], $isSaved ? 201 : 400);
            } else {

                return response()->json(['icon' => 'error', 'title' => $validatior->getMessageBag()->first()], 400);
            }



        //     return response()->json(['message' => $isSaved ? "Saved is Successfully" : "Saved is Fieald"] , $isSaved ? 200 : 400);

        // }
        // else{
        //     return response()->json(['message' => $validatior->getMessageBag()->first()] , 400);
        // }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function showshow($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $doctors = doctor::findOrFail($id);
        $specalities = specalities::all();
        $clinics = clinic::all();
        $rooms = room::all();
        return response()->view('cms.doctor.edit',compact('specalities','clinics','rooms','doctors'));
        // return response()->view('cms.doctor.edit' , compact('doctors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatedoctorRequest  $request
     * @param  \App\Models\doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatior = Validator($request->all(), [
            'name' => 'required|string|min:3|max:20',
            'email' => 'required|email',
            'phone' => 'required|string|min:5|max:20',
            'address' => 'required|string|min:5|max:20',
            'salary' => 'required|string|min:5|max:20',
            'cv' => 'required',
            'image' => 'required',
        ]);

        if (!$validatior->fails()) {
            // $doctors = new doctor();
            $doctors = doctor::findOrFail($id);
            $doctors->specalities_id = $request->get('specalities_id');
            $doctors->clinic_id = $request->get('clinic_id');
            $doctors->room_id = $request->get('room_id');
            $doctors->name = $request->get('name');
            $doctors->email = $request->get('email');
            $doctors->phone = $request->get('phone');
            $doctors->address = $request->get('address');
            $doctors->salary = $request->get('salary');


            if (request()->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time() . 'image.' . $image->getClientOriginalExtension();
                $image->move('images/doctor', $imageName);
                $doctors->image = $imageName;
            }
            if (request()->hasFile('cv')) {
                $cv= $request->file('cv');
                $fileName = time() . 'cv.' . $cv->getClientOriginalExtension();
                $cv->move('file/doctor', $fileName);
                $doctors->cv= $fileName;
            }
            $isSaved = $doctors->save();
            if ($isSaved) {
                // return ['redirect' => route('articles.index')];
                return response()->json(['icon' => 'success', 'title' => 'Updated Successfully'], $isSaved ? 201 : 400);
             } else {
                return response()->json(['icon' => 'error', 'title' => $validatior->getMessageBag()->first()], 400);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doctors = doctor::destroy($id);
        return response()->json(['icon' => 'success', 'title' => 'deleted Successfully'], $doctors ? 200 : 400);

    }
}
