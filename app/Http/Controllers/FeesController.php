<?php

namespace App\Http\Controllers;

use App\Models\fees;
use App\Http\Requests\StorefeesRequest;
use App\Http\Requests\UpdatefeesRequest;
// use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;



class FeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fees = fees::all();
           return response()->view('cms.fees.index',compact('fees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return response()->view('cms.fees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorefeesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fees'=>'required',
            'payment_of_methode'
        ]);
        $fees = new fees();
        $fees -> fees= $request->get('fees');
        $fees -> payment_of_methode= $request->get('payment');
        $issaved=$fees->save();
        return redirect()->route('fees.index');
        echo $issaved?"saved":"failed";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\fees  $fees
     * @return \Illuminate\Http\Response
     */
    public function show(fees $fees)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\fees  $fees
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fees=fees::findOrFail($id);
        return response()->view('cms.fees.edit',compact('fees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatefeesRequest  $request
     * @param  \App\Models\fees  $fees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'fees'=>'required',
            'payment_of_methode'
        ]);
        $fees = fees::findOrFail($id);
        $fees -> fees= $request->get('fees');
        $fees -> payment_of_methode= $request->get('payment');
        $issaved=$fees->save();
        return redirect()->route('fees.index');
        echo $issaved?"saved":"failed";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\fees  $fees
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fees = fees::destroy($id);
        return redirect()->back();
    }
}
