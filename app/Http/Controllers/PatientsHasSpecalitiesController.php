<?php

namespace App\Http\Controllers;

use App\Models\Patients_has_specalities;
use App\Http\Requests\StorePatients_has_specalitiesRequest;
use App\Http\Requests\UpdatePatients_has_specalitiesRequest;

class PatientsHasSpecalitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePatients_has_specalitiesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePatients_has_specalitiesRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Patients_has_specalities  $patients_has_specalities
     * @return \Illuminate\Http\Response
     */
    public function show(Patients_has_specalities $patients_has_specalities)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Patients_has_specalities  $patients_has_specalities
     * @return \Illuminate\Http\Response
     */
    public function edit(Patients_has_specalities $patients_has_specalities)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePatients_has_specalitiesRequest  $request
     * @param  \App\Models\Patients_has_specalities  $patients_has_specalities
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePatients_has_specalitiesRequest $request, Patients_has_specalities $patients_has_specalities)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Patients_has_specalities  $patients_has_specalities
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patients_has_specalities $patients_has_specalities)
    {
        //
    }
}
