<?php

namespace App\Http\Controllers;

use App\Models\city;
use App\Http\Requests\StorecityRequest;
use App\Http\Requests\UpdatecityRequest;
// use Illuminate\Http\Client\Request;
use  Illuminate\Http\Request;



class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = city::orderBy('id' , 'desc')->get();
        return response()->view('cms.city.index' , compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('cms.city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator($request->all(), [
            'name'=>"required|min:3|max:25",

        ]);

        if (!$validator->fails()) {
            $cities =new city();
            $cities->name = $request->get('name');
            $isSaved = $cities->save();

            return response()->json(['icon' => 'success', 'title' => 'saved is Successfully'], $isSaved ? 201 : 400);
        } else {

            return response()->json(['icon' => 'error', 'title' => $validator->getMessageBag()->first()], 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cities = city::findOrFail($id);
        return response()->view('cms.city.edit' , compact('cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator($request->all(), [
            'name'=>"required|min:3|max:25",
        ]);

        if (!$validator->fails()) {
            $cities =city::findOrFail($id);
            $cities->name = $request->get('name');

            $isSaved = $cities->save();
            return response()->json(['icon' => 'success', 'title' => 'update is Successfully'], $isSaved ? 201 : 400);
        } else {

            return response()->json(['icon' => 'error', 'title' => $validator->getMessageBag()->first()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cities = city::destroy($id);
        return response()->json(['message' => $cities ? "Deleted Successfully" : "Delete Failed"] , $cities ? 200 :400);
    }
}
