<?php

namespace App\Http\Controllers;

use App\Models\reception;
use App\Http\Requests\StorereceptionRequest;
use App\Http\Requests\UpdatereceptionRequest;
use App\Models\fees;
use GuzzleHttp\Psr7\Message;
use Illuminate\Auth\Events\Failed;
// use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;

class ReceptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recepion = reception::all();
           return response()->view('cms.recepion.index',compact('recepion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fees= fees::with('reception')->get();
        return response()->view('cms.recepion.create' , compact('fees'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  \App\Http\Requests\StorereceptionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

          $recepion = new reception();
          $recepion->fees_id= $request->get('fees_id');
          $recepion -> name= $request->get('name');
          $recepion -> phone= $request->get('phone');
          $recepion -> email= $request->get('email');
          $recepion -> date_of_booking= $request->get('date_of_booking');
          $issaved = $recepion->save();
          return redirect()->route('recepion.index');
          return response()->json(['message'=>$recepion ?"created successfully" : "Faield Created"],$issaved?200:400);

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\reception  $reception
     * @return \Illuminate\Http\Response
     */
    public function show(reception $reception)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\reception  $reception
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fees= fees::with('reception')->get();
        $recepion=reception::findOrFail($id);
        return response()->view('cms.recepion.edit',compact('recepion','fees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatereceptionRequest  $request
     * @param  \App\Models\reception  $reception
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'name'=>'required',
            'phone'=>'required',
            'email'=>'required',
            'data_of_booking'=>'required',
        ]);
        $recepion = reception::findOrFail($id);
        $recepion -> fees_id= $request->get('fees_id');
        $recepion -> name= $request->get('name');
        $recepion -> phone= $request->get('phone');
        $recepion -> email= $request->get('email');
        $recepion -> date_of_booking= $request->get('date_of_booking');
        $issaved = $recepion->save();
        return redirect()->route('recepion.index');
        return response()->json(['message'=>$recepion ?"created successfully" : "Faield Created"],$issaved?200:400);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\reception  $reception
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $recepion = reception::destroy($id);
        return redirect()->back();
    }
}
