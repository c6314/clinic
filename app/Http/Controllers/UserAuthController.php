<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserAuthController extends Controller
{
    public function showLogin($guard){
        return view('cms.auth.login' , compact('guard'));
    }

    public function login(Request $request){

        $validator = Validator($request->all(),[
          'email' => 'required|string|email',
          'password' => 'required|string|min:6',
        //   'remember' => 'boolean',
          'guard' => 'required|string|in:reception'
        ] ,[
            'email.required' => 'رجاء, أدخل البريد الإلكتروني',
            'email.email' => 'البريد الإلكتروني المدخل غير صحيح',
            'password.required' => 'رجاء, أدخل كلمة المرور',
            'guard.in' => 'تأكد من صحة رابط صفحة تسجيل الدخول'
        ]);

        $credenials =[
            'email' =>$request->get('email'),
            'password' =>$request->get('password'),
        ];

        if(!$validator->fails()){
            if(Auth::guard($request->get('guard'))->attempt($credenials,$request->get('remember'))){
                return response()->json(['message' => 'Login Successfully'] , 200);
            }
            else{
                return response()->json(['message' => 'Error Login '] , 400);
            }
        }
        return response()->json(['message' => $validator->getMessageBag()->first() ] , 400);

    }

    public function logout(Request $request){
        Auth::guard()->logout();
        $request->session()->invalidate();
        return redirect()->route('login.view' , 'admin');

    }
    public function resetPassword(){

    }
}
