<?php

namespace App\Http\Controllers;

use App\Models\rooms;
use App\Http\Requests\StoreroomRequest;
use App\Http\Requests\UpdateroomRequest;
use App\Models\city;
use App\Models\clinic;
use App\Models\doctor;
use App\Models\fees;
use App\Models\room;
use App\Models\reception;


use Illuminate\Http\Request;


class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *

     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = room::all();
        return response()->view('cms.room.index' , compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // $roles = reception::with('reception')->with('clinic')->with('room')->get();
        $reception = reception::all();
        $clinics = clinic::all();
        $rooms = room::all();
        $doctors = doctor::all();
        $rooms = room::all();
        $receptions=reception::all();
        $fees=fees::all();
        $cities=city::all();

        return response()->view('cms.room.create' ,
        compact('reception','clinics','rooms','doctors','receptions','fees','cities'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreroomRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validatior = Validator($request->all(),[
            'room_type' => 'required|string|min:3|max:20',
        ]);

        if(!$validatior->fails()){
            $rooms = new room();
            $rooms->clinic_id = $request->get('clinic_id');
            $rooms->reception_id = $request->get('reception_id');
            $rooms->room_type = $request->get('room_type');

            $isSaved = $rooms->save();
            return response()->json(['icon' => 'success', 'title' => 'saved is Successfully'], $isSaved ? 201 : 400);
        } else {

            return response()->json(['icon' => 'error', 'title' => $validatior->getMessageBag()->first()], 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\room  $room
     * @return \Illuminate\Http\Response
     */
    public function showshow($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rooms = room::findOrFail($id);
        $receptions = reception::all();
        $clinics = clinic::all();
        return response()->view('cms.room.edit',compact('receptions','clinics','rooms'));

    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateroomRequest  $request
     * @param  \App\Models\room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){


        $validatior = Validator($request->all(),[
            // 'room_type' => 'required|string|min:3|max:20',
        ]);

    if(!$validatior->fails()){
        $rooms = room::findOrFail($id);
            $rooms->room_type = $request->get('room_type');
            $rooms->clinic_id = $request->get('clinic_id');
            $rooms->reception_id = $request->get('reception_id');


        $isSaved = $rooms->save();
        return response()->json(['icon' => 'success', 'title' => 'update is Successfully'], $isSaved ? 201 : 400);
    } else {

        return response()->json(['icon' => 'error', 'title' => $validatior->getMessageBag()->first()], 400);
    }

}


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rooms = room::destroy($id);
        return response()->json(['message' => $rooms ? "Deleted is Successfully" : "Deleted is Fieald"] ,  $rooms ? 200 : 400);
    }
}
