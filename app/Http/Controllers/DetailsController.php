<?php


namespace App\Http\Controllers;
use App\Models\details;
use App\Http\Requests\StoredetailsRequest;
use App\Http\Requests\UpdatedetailsRequest;
use  Illuminate\Http\Request;

class DetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = details::orderBy('id' , 'desc')->get();
        return response()->view('cms.Details.index' , compact('details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('cms.Details.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator($request->all(), [
            'status'=>"required",
            'description'=>"required|min:3|max:25",
        ]);

        if (!$validator->fails()) {
            $details =new details();
            $details->status = $request->get('status');
            $details->description = $request->get('description');

            $isSaved = $details->save();

            return response()->json(['icon' => 'success', 'title' => 'saved is Successfully'], $isSaved ? 201 : 400);
        } else {

            return response()->json(['icon' => 'error', 'title' => $validator->getMessageBag()->first()], 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $details = details::findOrFail($id);
        return response()->view('cms.Details.edit' , compact('details'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator($request->all(), [
            'status'=>"required|min:3|max:25",
            'description'=>"required|min:3|max:25",


        ]);

        if (!$validator->fails()) {
            $details =details::findOrFail();
            $details->status = $request->get('status');
            $details->description = $request->get('description');
            $isSaved = $details->save();
            return response()->json(['icon' => 'success', 'title' => 'update is Successfully'], $isSaved ? 201 : 400);
        } else {

            return response()->json(['icon' => 'error', 'title' => $validator->getMessageBag()->first()], 400);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $details = details::destroy($id);
        return response()->json(['message' => $details ? "Deleted Successfully" : "Delete Failed"] , $details ? 200 :400);
    }
}
