<?php

namespace App\Http\Controllers;

use App\Models\specalities;
use App\Http\Requests\StorespecalitiesRequest;
use App\Http\Requests\UpdatespecalitiesRequest;
use App\Models\clinic;
// use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;

class SpecalitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specality = specalities::all();
        return response()->view('cms.spaecality.index',compact('specality'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clinic= clinic::with('specalities')->get();
        return response()->view('cms.spaecality.create',compact('clinic'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorespecalitiesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $specality = new specalities();
        $specality -> clinic_id= $request->get('clinic_id');
        $specality -> name= $request->get('name');
        $specality -> description= $request->get('description');
        $issaved = $specality->save();
        return redirect()->route('spec.index');
        return response()->json(['message'=>$specality ?"created successfully" : "Faield Created"],$issaved?200:400);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\specalities  $specalities
     * @return \Illuminate\Http\Response
     */
    public function show(specalities $specalities)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\specalities  $specalities
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clinic= clinic::with('specalities')->get();
        $specality=specalities::findOrFail($id);
        return response()->view('cms.spaecality.edit',compact('specality','clinic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatespecalitiesRequest  $request
     * @param  \App\Models\specalities  $specalities
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $specality = specalities::findOrFail($id);
        $specality -> name= $request->get('name');
        $specality -> description= $request->get('description');
        $issaved = $specality->save();
        return redirect()->route('spec.index');
        return response()->json(['message'=>$specality ?"created successfully" : "Faield Created"],$issaved?200:400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\specalities  $specalities
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $specality = specalities::destroy($id);
        return redirect()->back();
    }
}
