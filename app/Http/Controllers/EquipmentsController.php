<?php

namespace App\Http\Controllers;

use App\Models\equipments;
use App\Http\Requests\StoreequipmentsRequest;
use App\Http\Requests\UpdateequipmentsRequest;
use App\Models\clinic;
use App\Models\room;
// use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;


class EquipmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $equipments = equipments::all();
        return response()->view('cms.equipments.index',compact('equipments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $room= room::with('equipments')->get();
        $clin=clinic::with('equipments')->get();
        return response()->view('cms.equipments.create',compact('room','clin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreequipmentsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $equipments=new equipments();
        $equipments -> room_id= $request->get('room_id');
        $equipments -> clinics_id= $request->get('clinics_id');
        $equipments -> name= $request->get('name');
        $issaved = $equipments->save();
        return redirect()->route('equ.index');
        return response()->json(['message'=>$equipments ?"created successfully" : "Faield Created"],$issaved?200:400);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\equipments  $equipments
     * @return \Illuminate\Http\Response
     */
    public function show(equipments $equipments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\equipments  $equipments
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clin=clinic::with('equipments')->get();
        $room= room::with('equipments')->get();
        $equipments=equipments::findOrFail($id);
        return response()->view('cms.equipments.edit',compact('equipments','room','clin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateequipmentsRequest  $request
     * @param  \App\Models\equipments  $equipments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $equipments=equipments::findOrFail($id);
        $equipments -> name= $request->get('name');
        $issaved = $equipments->save();
        return redirect()->route('equ.index');
        return response()->json(['message'=>$equipments ?"created successfully" : "Faield Created"],$issaved?200:400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\equipments  $equipments
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $equipments=equipments::destroy($id);
        return redirect()->back();
    }
}
