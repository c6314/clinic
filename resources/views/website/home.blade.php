<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="discription" content="">
    <title></title>
    <!-- this is icons 4.7 -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- this is owl carousel -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- this is select2 -->
    <link rel="stylesheet" href="css/select2.css">
    <!-- this is bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- this is rtl
    <link rel="stylesheet" href="css/bootstrap-rtl.min.css"> -->
    <!-- this is css -->
    <link rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
   <section class="sec">
     <div class="container">
       <!-- start navbar -->
      <section class="navbar">
      <!-- navbar-fixed-top لجعل االناف ثابت في اعلى الصفحة  -->
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            {{-- <i  aria-hidden="true"> </i> --}}
            {{-- <img src="image/12.png" alt=""> --}}
            <i class="fa fa-heartbeat" aria-hidden="true">clinic</i>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                  <!--  معمول للتقسيم بقدر احذفه مو الزامي divider -->
                  <li role="separator" class="divider"></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                  <!--  معمول للتقسيم بقدر احذفه مو الزامي divider -->
                  <li role="separator" class="divider"></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                  <!--  معمول للتقسيم بقدر احذفه مو الزامي divider -->
                  <li role="separator" class="divider"></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li>
              <li><a href="#">Features</a></li>
              <li><a href="#">Pricing</a></li>
              <li><a href="#">FAQs</a></li>
              <li><button class="btn btn_info ii">Lets started</button></li>
            </ul>

          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
     </section>
    <!-- end navbar -->
     </div>

   </section>

   <div class="gray-sec">
    <div class="box-a">
        {{-- <i class="fa fa-user"></i> --}}
        <h3>Request an appointment</h3>
    </div>
    <div class="box-b">
        {{-- <i class="fa fa-user"></i> --}}
        <h3>Search diseases </h3>
    </div>
    <div class="box-c">
        {{-- <i class="fa fa-user"></i> --}}
        <h3> Find a doctor</h3>
    </div>
    {{-- <div class="box-d">
        <i class="fa fa-user"></i>
        <h3>امتلك محتوى الدورات مدى الحياة</h3>
    </div> --}}

 </div>
<div class="container a">
 <div class="row">
    <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <img src="image/b.jpg" alt="...">
        <div class="caption">
          <h3>More experience</h3>
          <p>The million patients we treat each year prepares us to treat the one who matters most—you.</p>
          <p><a href="#" class="btn btn-default" role="button">Button</a></p>
        </div>
      </div>
    </div>


    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
          <img src="image/b.jpg"  alt="...">
          <div class="caption">
            <h3>The right answers</h3>
            <p>Count on our experts to deliver an accurate diagnosis and the right plan for you the first time.</p>
            <p><a href="#" class="btn btn-default" role="button">Button</a></p>
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
          <img src="image/b.jpg"  alt="...">
          <div class="caption">
            <h3>You come first</h3>
            <p>Treatment at Mayo Clinic is a truly human experience. You’re cared for as a person first.</p>
            <p><a href="#" class="btn btn-default" role="button">Button</a></p>
          </div>
        </div>
      </div>
  </div>



</div>
<div class="container a">
    <div class="row">
       <div class="col-sm-6 col-md-4">
         <div class="thumbnail">
           <img src="image/b.jpg" alt="...">
           <div class="caption">
             <h3>More experience</h3>
             <p>The million patients we treat each year prepares us to treat the one who matters most—you.</p>
             <p><a href="#" class="btn btn-default" role="button">Button</a></p>
           </div>
         </div>
       </div>


       <div class="col-sm-6 col-md-4">
           <div class="thumbnail">
             <img src="image/b.jpg"  alt="...">
             <div class="caption">
               <h3>The right answers</h3>
               <p>Count on our experts to deliver an accurate diagnosis and the right plan for you the first time.</p>
               <p><a href="#" class="btn btn-default" role="button">Button</a></p>
             </div>
           </div>
         </div>

         <div class="col-sm-6 col-md-4">
           <div class="thumbnail">
             <img src="image/b.jpg"  alt="...">
             <div class="caption">
               <h3>You come first</h3>
               <p>Treatment at Mayo Clinic is a truly human experience. You’re cared for as a person first.</p>
               <p><a href="#" class="btn btn-default" role="button">Button</a></p>
             </div>
           </div>
         </div>
     </div>



   </div>
  <footer>
      <section class="footer">
      <div class="icons">
        <i class="fa fa-google-plus-square" aria-hidden="true"></i>
        <i class="fa fa-google-plus-square" aria-hidden="true"></i>
        <i class="fa fa-google-plus-square" aria-hidden="true"></i>
        <i class="fa fa-google-plus-square" aria-hidden="true"></i>
      </section>
        </div>
  </footer>
    <script src="js/jquery.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>
