@extends('cms.parent')

@section('title' , 'Clinic')

@section('main-title' , 'Create Clinic')

@section('small-title' , 'Clinics')

@section('styles')
<link rel="stylesheet" href="{{ asset('cms/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">

@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Clinic</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="create_form">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                             <div class="col-md-6">
                                    <div class="form-group">
                                      <label>City</label>
                                      <select class="form-control select2" name="city_id" style="width: 100%;" id="city_id">
                                        @foreach ($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label>Reception</label>
                                      <select class="form-control select2" name="reception_id" style="width: 100%;" id="reception_id">
                                        @foreach ($receptions as $reception)
                                        <option value="{{ $reception->id }}">{{ $reception->name }}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                </div>

                              </div>

                            <div class="row">

                            <div class="form-group col-md-4">
                                <label for="name"> name</label>
                                <input type="text" class="form-control" name="name" id="name"
                                    placeholder="Enter your  name">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="phone">phone</label>
                                <input type="text" class="form-control" name="phone" id="phone"
                                    placeholder="Enter youe phone number ">
                            </div>

                </div>
                        </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="button" onclick="performStore()" class="btn btn-primary">Store</button>
                    <a href="{{ route('clinics.index') }}" type="button"  class="btn btn-success">Return Back</a>

                </div>
                </form>
            </div>



</section>
@endsection
{{-- <script src="{{ asset('cms/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script> --}}

@section('scripts')

    <script>
        function performStore(){
            let data ={
                name: document.getElementById('name').value,
                phone: document.getElementById('phone').value,
            }
            store('/cms/clinics',data);
        }
    </script>

@endsection
