@extends('cms.parent')

@section('title' , 'Clinic')

@section('main-title' , 'Edit Clinic')

@section('small-title' , 'Clinics')

@section('styles')

@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Clinic</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="create_form">
                        @csrf
                        <div class="card-body">
                            <div class="row">

                            <div class="form-group col-md-4">
                                <label for="name">name</label>
                                <input type="text" class="form-control" name="name" id="name"
                                    value="{{ $clinics->name }}" placeholder="Enter your  name">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="phone">phone</label>
                                <input type="text" class="form-control" name="phone" id="phone"
                                value="{{ $clinics->phone }}" placeholder="Enter your phone ">
                            </div>


                </div>
                <!-- /.card-body -->

                    <div class="card-footer">

                    <button type="button" onclick="performUpdate({{ $clinics->id }})" class="btn btn-primary">Update</button>
                    <a href="{{ route('doctor.index') }}" type="button"  class="btn btn-success">Return Back</a>

                </div>
                </form>
            </div>



</section>
@endsection

@section('scripts')

<script>
    function performUpdate(id){
        let data ={
            name: document.getElementById('name').value,
            phone: document.getElementById('phone').value,

        }
        let redirectUlr = '/cms/clinics'
        update('/cms/clinics/'+ id, data , redirectUlr);

    }

</script>
@endsection
