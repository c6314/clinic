@extends('cms.parent')

@section('title' , 'Clinic')

@section('main-title' , 'Index Clinic')

@section('small-title' , 'clinic')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header ">
          <h3 class="card-title"></h3>
                <a href="{{ route('clinics.create') }}" type="button"  class="btn btn-success">Add new clinic</a>

          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table  table-bordered table-striped table-hover text-nowrap">
            <thead>
              <tr>
                <th>ID</th>
                <th> name</th>
                <th> phone</th>
                <th>Created At</th>
                <th>Setting</th>

              </tr>
            </thead>
            <tbody>
                {{-- <td><span class="tag tag-success">Approved</span></td> --}}
                @foreach ($clinics as $clinic )
                <tr>
                    <td>{{ $clinic->id }}</td>
                    <td>{{ $clinic->name }}</td>
                    <td>{{ $clinic->phone }}</td>
                    <td>{{ $clinic->created_at }}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ route('clinics.edit', $clinic->id) }}" class="btn btn-info">
                              <i class="fas fa-edit"></i>
                            </a>


                        <a href="#" onclick="performDestroy({{ $clinic->id }}, this)" class="btn btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </a>

                        </div>
                    </td>
                  </tr>
                @endforeach


            </tbody>
          </table>
        </div>
    </div>
</div>

      </div>
    </div>

    </div>
  </div>
@endsection

@section('scripts')

<script>
    function performDestroy(id , ref){
    confirmDestroy('/cms/clinics/'+id ,ref);
    }
</script>
@endsection
