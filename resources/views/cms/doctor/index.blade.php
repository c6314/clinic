@extends('cms.parent')

@section('title' , 'doctor')

@section('main-title' , 'Index doctor')

@section('small-title' , 'doctor')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header ">
          <h3 class="card-title"></h3>
                <a href="{{ route('doctor.create') }}" type="button"  class="btn btn-success">Add new doctor</a>
          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table  table-bordered table-striped table-hover text-nowrap">
            <thead>
              <tr>

                <th>ID</th>
                <th>Full Name</th>
                <th>image</th>
                <th>phone</th>
                <th>Email</th>
                <th>address</th>
                <th>salary</th>
                <th>cv</th>
                <th>specalities</th>
                <th>clinic</th>

                <th>room</th>

                {{-- <th>Created At</th> --}}
                <th>Setting</th>
              </tr>
            </thead>
            <tbody>
                {{-- <td><span class="tag tag-success">Approved</span></td> --}}
                @foreach ($doctors as $doctor )
                <tr>
                    <td>{{ $doctor->id }}</td>
                    <td>{{ $doctor->name }}</td>
                    {{-- <td>{{ $doctor->image }}</td> --}}
                    <td> <img class="img-circle img-bordered-sm" src="{{asset('images/doctor/'.$doctor->image)}}" width="50" height="50" alt="User Image"></td>

                    <td>{{ $doctor->phone }}</td>
                    <td>{{ $doctor->email }}</td>
                    <td>{{ $doctor->address }}</td>
                    <td>{{ $doctor->salary }}</td>
                    {{-- <td>{{ $doctor->cv }}</td> --}}
                    <td> <img class="img-circle img-bordered-sm" src="{{asset('file/doctor/'.$doctor->cv)}}" width="50" height="50" alt="User Image"></td>
                    {{-- <td>{{ $doctor->salary }}</td> --}}

                    {{-- <td>{{ $doctor->specalities_id }}</td> --}}
                    <td>{{ $doctor->specalities ? $doctor->specalities->name : 'NULL' }}</td>
                    <td>{{ $doctor->clinic ? $doctor->clinic->name : 'NULL' }}</td>
                    <td>{{ $doctor->rooms ? $doctor->rooms->room_type : 'NULL' }}</td>

                    <td>
                        <div class="btn-group">
                            {{-- @can('Edit-doctor') --}}

                            {{--
                            {{-- @endcan --}}
                            {{-- @can('Delete-doctor') --}}
                            <a href="{{ route('doctor.edit', $doctor->id) }}" class="btn btn-info">
                                <i class="fas fa-edit"></i>
                              </a>

                        <a href="#" onclick="performDestroy({{ $doctor->id }}, this)" class="btn btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        {{-- @endcan --}}

                    </td>
                  </tr>
                @endforeach


            </tbody>
          </table>
        </div>
    </div>
</div>

        <!-- /.card-body -->
      </div>
    </div>
    {{-- {{ $doctors->links() }} --}}

      <!-- /.card -->
    </div>
  </div>
@endsection

@section('scripts')

<script>
    function performDestroy(id , ref){
        confirmDestroy('/cms/doctor/'+id ,ref);
    }
</script>
@endsection
