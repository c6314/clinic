@extends('cms.parent')

@section('title' , 'Equipments')

@section('main-title' , 'Edit Equipments')

@section('small-title' , 'Equipments')

@section('styles')

@endsection

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Quick Example</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route('equ.store')}}" method="POST">
            @csrf
          <div class="card-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                      <label>room</label>
                      <select class="form-control select2" name="room_id" style="width: 100%;" id="room_id">
                        @foreach ($room as $rooms)
                        <option value="{{ $rooms->id }}"></option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                      <label>clinic</label>
                      <select class="form-control select2" name="clinic_id" style="width: 100%;" id="clinic_id">
                        @foreach ($clin as $clinics)
                        <option value="{{ $clinics->id }}">{{ $clinics->name}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

            </div>
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" value={{ $equipments->name }} placeholder="Enter name">
            </div>
            <div class="form-group">
              <label for="phone">Phone</label>
              <input type="text" class="form-control" id="phone" name="phone" value={{ $equipments->phone }} placeholder="Enter phone">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" value={{ $equipments->email }} placeholder="Enter email">
              </div>
              <div class="form-group">
                <label for="booking">Date_Of_Booking</label>
                <input type="text" class="form-control" id="booking" name="date_of_booking" value={{ $equipments->date_of_booking }} placeholder="Enter date_of_booking">
              </div>


          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
      <!-- /.card -->


@endsection

@section('scripts')

@endsection
