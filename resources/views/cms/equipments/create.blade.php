@extends('cms.parent')

@section('title' , 'Equipments')

@section('main-title' , 'Create Equipments')

@section('small-title' , 'Equipments')

@section('styles')
<link rel="stylesheet" href="{{ asset('cms/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Create Equipments</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route('equ.store')}}" method="POST">
            @csrf
          <div class="card-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                      <label>room</label>
                      <select class="form-control select2" name="room_id" style="width: 100%;" id="room_id">
                        @foreach ($room as $rooms)
                        <option value="{{ $rooms->id }}"></option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                      <label>clinic</label>
                      <select class="form-control select2" name="clinic_id" style="width: 100%;" id="clinic_id">
                        @foreach ($clin as $clinics)
                        <option value="{{ $clinics->id }}">{{ $clinics->name}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

            </div>
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
            </div>
            <div class="form-group">
              <label for="phone">Phone</label>
              <input type="text" class="form-control" name="phone" id="phone" placeholder="Enter phone">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="Enter email">
              </div>
              <div class="form-group">
                <label for="date">Date_Of_Booking</label>
                <input id="date" type="date"  name="date_of_booking" class="form-control"
                    placeholder="date of booking">
            </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Store</button>
            <a href="{{ route('equ.index')}}" type="button"  class="btn btn-primary">Return Back</a>

        </div>
        </form>
      </div>
      <!-- /.card -->


@endsection

@section('scripts')
<script src="{{ asset('cms/plugins/select2/js/select2.full.min.js') }}"></script>

@endsection
