@extends('cms.parent')

@section('title' , 'Equipments')

@section('main-title' , 'Index Equipments')

@section('small-title' , 'Equipments')

@section('styles')

@endsection

@section('content')
<!-- /.row -->
<div class="row">
  <div class="col-12">
    <div class="card table table-bordered">
      <div class="card-header">
        <h3 class="card-title">Index Equipments</h3>

        <div class="card-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
          <thead>
            <tr>
              <th>ID</th>
              <th>name</th>
              <th>created_at</th>
              <th>updated_at</th>
              <th>setting</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($equipments as $equipment )
                <tr>
                  <td>{{ $equipment->id }}</td>
                  <td>{{ $equipment->name }}</td>
                  <td>{{ $equipment->created_at }}</td>
                  <td>{{ $equipment->updated_at }}</td>
                  <td>
                    <div class="btn-group">
                        <a href="{{ route('equ.edit', $equipment->id) }}" class="btn btn-info">
                          <i class="fas fa-edit"></i>
                        </a>
                        <form action="{{ route('equ.destroy' , $equipment->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                          <button type="submit" class="btn btn-danger">
                           <i class="fas fa-trash-alt"></i>
                          </button>
                        </form>

                   </div>
                  </td>
                 {{--  <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>  --}}
               </tr>
              @endforeach

          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>
<!-- /.row -->
@endsection

@section('scripts')

@endsection
