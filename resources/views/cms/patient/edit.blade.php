@extends('cms.parent')

@section('title', 'patient')

@section('main-title', 'Edite patient')

@section('small-title', 'patients')

@section('styles')
    <link rel="stylesheet" href="{{ asset('cms/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('cms/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">


@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Create patient</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="create_form">
                            @csrf
                            <div class="card-body">
                                <div class="row">

                                    <div class="form-group col-md-4">
                                        <label for="name">name</label>
                                        <input type="text" class="form-control" name="name" id="name"
                                            value="{{ $patients->name }}" placeholder="Enter your name">
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="phone">phone</label>
                                        <input type="text" class="form-control" name="phone" id="phone"
                                            value="{{ $patients->phone }}" placeholder="Enter your phone">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="age">age</label>
                                        <input type="text" class="form-control" name="age" id="age"
                                            value="{{ $patients->age }}" placeholder="Enter your age">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="address">address</label>
                                        <input type="text" class="form-control" name="address" id="address"
                                            value="{{ $patients->address }}" placeholder="Enter your address">
                                    </div>


                                    <div class="form-group col-md-4">
                                        <label for="gender">gender</label>
                                    <select class="form-select form-select-sm" name="gender" style="width: 100%;"
                                    id="gender" aria-label=".form-select-sm example">
                                    {{-- <option selected></option> --}}
                                    <option value="{{ $patients->gender }}">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>


                                    <div class="form-group col-md-4">
                                        <label>doctors</label>
                                        <select class="form-control select2" style="width: 100%;" name="doctor_id"
                                            id="doctor_id">
                                            {{-- <option selected="selected">Alabama</option> --}}
                                            @foreach ($doctors as $role)
                                                <option value={{ $role->id }}>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="form-group col-md-4">
                                        <label>clinic</label>
                                        <select class="form-control select2" style="width: 100%;" name="clinic_id"
                                            id="clinic_id">
                                            @foreach ($clinics as $role)
                                                <option value={{ $role->id }}>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="form-group col-md-4">
                                        <label>room</label>
                                        <select class="form-control select2" style="width: 100%;" name="room_id"
                                            id="room_id">
                                            @foreach ($rooms as $role)
                                                <option value={{ $role->id }}>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>



                                    <div class="form-group col-md-4">
                                        <label>receptions</label>
                                        <select class="form-control select2" style="width: 100%;" name="reception_id"
                                            id="reception_id">
                                            @foreach ($receptions as $role)
                                                <option value={{ $role->id }}>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label>fees</label>
                                        <select class="form-control select2" style="width: 100%;" name="fees_id"
                                            id="fees_id">
                                            @foreach ($fees as $role)
                                                <option value={{ $role->id }}>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label>city</label>
                                        <select class="form-control select2" style="width: 100%;" name="city_id"
                                            id="city_id">
                                            @foreach ($cities as $role)
                                                <option value={{ $role->id }}>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>



                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">

                                <button type="button" onclick="performUpdate({{ $patients->id }})"
                                    class="btn btn-primary">Update</button>
                                <a href="{{ route('patient.index') }}" type="button" class="btn btn-success">Return
                                    Back</a>

                            </div>
                        </form>
                    </div>



    </section>
@endsection

@section('scripts')
    <script src="{{ asset('cms/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('cms/plugins/select2/js/select2.full.min.js') }}"></script>



    <script>
        function performUpdate(id) {
            let data ={
            doctor_id: document.getElementById('doctor_id').value,
            room_id: document.getElementById('room_id').value,
            clinic_id: document.getElementById('clinic_id').value,

            reception_id: document.getElementById('reception_id').value,
            fees_id: document.getElementById('fees_id').value,
            city_id: document.getElementById('city_id').value,

            name: document.getElementById('name').value,
            age: document.getElementById('age').value,
            phone: document.getElementById('phone').value,

            address: document.getElementById('address').value,
            gender: document.getElementById('gender').value,
            }
        let redirectUlr = '/cms/patient'
        update('/cms/patient/'+ id, data , redirectUlr);

    }
    </script>

@endsection
