@extends('cms.parent')

@section('title' , 'Fees')

@section('main-title' , 'Index Fees')

@section('small-title' , ' Fees')

@section('styles')

@endsection

@section('content')
<!-- /.row -->
<div class="row">
  <div class="col-12">
    <div class="card table table-bordered">
      <div class="card-header">
        <h3 class="card-title">index fees</h3>

        <div class="card-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0" style="height: 300px;">
        <table class="table table-head-fixed text-nowrap">
          <thead>
            <tr>
              <th>ID</th>
              <th>fees</th>
              <th>payment_of_methode</th>
              <th>created_at</th>
              <th>updated_at</th>
              <th>setting</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($fees as $fee )
                <tr>
                  <td>{{ $fee->id }}</td>
                  <td>{{ $fee->fees }}</td>
                  <td>{{ $fee->payment_of_methode}}</td>
                  <td>{{ $fee->created_at }}</td>
                  <td>{{ $fee->updated_at}}</td>
                  <td>
                    <div class="btn-group">
                        <a href="{{ route('fees.edit',$fee->id)}}" class="btn btn-info">
                          <i class="fas fa-edit"></i>
                        </a>
                        <form action="{{ route('fees.destroy', $fee->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                           <button type="submit" class="btn btn-danger">
                             <i class="fas fa-trash-alt"></i>
                           </button>
                        </form>

                   </div>
                  </td>
            </tr>
              @endforeach

          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>
<!-- /.row -->

@endsection

@section('scripts')

@endsection
