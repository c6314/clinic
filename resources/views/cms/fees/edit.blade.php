@extends('cms.parent')

@section('title' , 'Fees')

@section('main-title' , 'Edit Fees')

@section('small-title' , 'Fees')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Quick Example</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route('fees.store')}}" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="fees">Fees</label>
              <input type="text" class="form-control" id="fees" name="fees" value="{{ $fees->fees }}" placeholder="Enter fees">
            </div>
            <div class="form-group">
              <label for="payment">payment_of_methode</label>
              <input type="text" class="form-control" id="payment" name="payment" value="{{ $fees->payment_of_methode}}" placeholder="payment_of_methode">
            </div>

          <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-info">Update</button>
             </div>


          <!-- /.card-footer -->
        </form>
      </div>
      <!-- /.card -->
@endsection

@section('scripts')

@endsection
