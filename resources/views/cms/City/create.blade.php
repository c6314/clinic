@extends('cms.parent')

@section('name' , 'City')

@section('main-name' , 'Create City')

@section('small-name' , 'City')

@section('styles')

@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-name">Create City</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="create_form">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                {{-- <input type="text" name="author_id" id="author_id" value="{{$id}}"
                            class="form-control form-control-solid" hidden/> --}}

                            <div class="form-group col-md-6">
                                <label for="name">name</label>
                                <input type="text" class="form-control" name="name" id="name"
                                    placeholder="Enter your  name">
                            </div>



                </div>
                        </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="button" onclick="performStore()" class="btn btn-primary">Store</button>
                    <a href="{{ route('cities.index') }}" type="button"  class="btn btn-primary">Return Back</a>

                </div>
                </form>
            </div>



</section>
@endsection

@section('scripts')
    <script>
        function performStore(){
            let data ={
                name: document.getElementById('name').value,
            }
            store('/cms/cities',data);
        }
    </script>
@endsection
