@extends('cms.parent')

@section('name' , 'City')

@section('main-name' , 'Edit City')

@section('small-name' , 'City')

@section('styles')

@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-name">Edit City</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="create_form">
                        @csrf
                        <div class="card-body">
                            <div class="row">

                            <div class="form-group col-md-4">
                                <label for="name">name</label>
                                <input type="text" class="form-control" name="name" id="name"
                                    value="{{ $cities->name }}" placeholder="Enter your  name">
                            </div>



                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="button" onclick="performUpdate({{ $cities->id }})" class="btn btn-primary">Update</button>
                </div>
                </form>
            </div>



</section>
@endsection

@section('scripts')

<script>
    function performUpdate(id){
        let data ={
            name: document.getElementById('name').value,

        }
        let redirectUlr = '/cms/cities'
        update('/cms/cities/'+ id , data ,redirectUlr);

    }

</script>
@endsection
