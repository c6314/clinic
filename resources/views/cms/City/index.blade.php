@extends('cms.parent')

@section('name' , 'City')

@section('main-name' , 'Index City')

@section('small-name' , 'City')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header ">
          <h3 class="card-name"></h3>
          <a href="{{ route('cities.create') }}" type="button"  class="btn btn-primary">Add new City</a>


          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table  table-bordered table-striped table-hover text-nowrap">
            <thead>
              <tr>
                <th>ID</th>
                <th>name</th>
                <th>Setting</th>
              </tr>
            </thead>
            <tbody>
                {{-- <td><span class="tag tag-success">Approved</span></td> --}}
                @foreach ($cities as $City )
                <tr>
                    <td>{{ $City->id }}</td>
                    <td>{{ $City->name }}</td>


                      <td>
                        <div class="btn-group">
                            <a href="{{ route('cities.edit', $City->id) }}" class="btn btn-info">
                              <i class="fas fa-edit"></i>
                            </a>

                        <a href="#" onclick="performDestroy({{ $City->id }}, this)" class="btn btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        </div>
                    </td>
                  </tr>
                @endforeach


            </tbody>
          </table>
        </div>
    </div>

        <!-- /.card-body -->
      </div>
    </div>

      <!-- /.card -->
    </div>
  </div>
@endsection

@section('scripts')

<script>
    function performDestroy(id , ref){
        confirmDestroy('/cms/cities/'+id ,ref);
    }
</script>
@endsection
