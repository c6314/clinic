@extends('cms.parent')

@section('title' , 'employee')

@section('main-title' , 'Index employee')

@section('small-title' , 'employee')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header ">
          <h3 class="card-title"></h3>
                <a href="{{ route('employee.create') }}" type="button"  class="btn btn-success">Add new employee</a>
          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table  table-bordered table-striped table-hover text-nowrap">
            <thead>
              <tr>

                <th>ID</th>
                <th>Full Name</th>
                <th>image</th>
                <th>phone</th>
                {{-- <th>Email</th> --}}
                <th>address</th>
                {{-- <th>salary</th> --}}
                <th>cv</th>
                <th>specalities</th>
                <th>clinic</th>

                <th>room</th>

                {{-- <th>Created At</th> --}}
                <th>Setting</th>
              </tr>
            </thead>
            <tbody>
                {{-- <td><span class="tag tag-success">Approved</span></td> --}}
                @foreach ($employees as $employee )
                <tr>
                    <td>{{ $employee->id }}</td>
                    <td>{{ $employee->name }}</td>
                    {{-- <td>{{ $employee->image }}</td> --}}
                    <td> <img class="img-circle img-bordered-sm" src="{{asset('images/employee/'.$employee->image)}}" width="50" height="50" alt="User Image"></td>

                    <td>{{ $employee->phone }}</td>
                    {{-- <td>{{ $employee->email }}</td> --}}
                    <td>{{ $employee->address }}</td>
                    {{-- <td>{{ $employee->salary }}</td> --}}
                    {{-- <td>{{ $employee->cv }}</td> --}}
                    <td> <img class="img-circle img-bordered-sm" src="{{asset('file/employee/'.$employee->cv)}}" width="50" height="50" alt="User Image"></td>

                    {{-- <td>{{ $employee->specalities_id }}</td>
                    <td>{{ $employee->clinic_id }}</td>
                    <td>{{ $employee->room_id }}</td> --}}

                    <td>{{ $employee->specalities ? $employee->specalities->name : 'NULL' }}</td>
                    <td>{{ $employee->clinic ? $employee->clinic->name : 'NULL' }}</td>
                    <td>{{ $employee->rooms ? $employee->rooms->room_type : 'NULL' }}</td>
                    <td>
                        <div class="btn-group">
                            {{-- @can('Edit-employee') --}}

                            {{--
                            {{-- @endcan --}}
                            {{-- @can('Delete-employee') --}}
                            <a href="{{ route('employee.edit', $employee->id) }}" class="btn btn-info">
                                <i class="fas fa-edit"></i>
                              </a>

                        <a href="#" onclick="performDestroy({{ $employee->id }}, this)" class="btn btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        {{-- @endcan --}}

                    </td>
                  </tr>
                @endforeach


            </tbody>
          </table>
        </div>
    </div>
</div>

        <!-- /.card-body -->
      </div>
    </div>
    {{-- {{ $employees->links() }} --}}

      <!-- /.card -->
    </div>
  </div>
@endsection

@section('scripts')

<script>
    function performDestroy(id , ref){
        confirmDestroy('/cms/employee/'+id ,ref);
    }
</script>
@endsection
