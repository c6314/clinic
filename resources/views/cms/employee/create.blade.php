@extends('cms.parent')

@section('title' , 'employee')

@section('main-title' , 'Create employee')

@section('small-title' , 'employees')

@section('styles')
<link rel="stylesheet" href="{{ asset('cms/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('cms/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">


@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create employee</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="create_form">
                        @csrf
                        <div class="card-body">

                            <div class="row">

                                <div class="col-md-6">

                                </div>
                            </div>

                            <div class="row">

                            <div class="form-group col-md-4">
                                <label for="name">name</label>
                                <input type="text" class="form-control" name="name" id="name"
                                    placeholder="Enter your name">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="phone">phone</label>
                                <input type="text" class="form-control" name="phone" id="phone"
                                    placeholder="Enter your phone">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="address">address</label>
                                <input type="text" class="form-control" name="address" id="address"
                                    placeholder="Enter your address">
                            </div>


                            <div class="form-group col-md-4">
                                <label>cities</label>
                                <select class="form-control select2" style="width: 100%;" name="city_id" id="city_id">
                                  {{-- <option selected="selected">Alabama</option> --}}
                                  @foreach($cities as $role)
                                  <option value={{ $role->id}}>{{ $role->name }}</option>
                                @endforeach
                                </select>
                            </div>


                            <div class="form-group col-md-4">
                                <label>clinic</label>
                                <select class="form-control select2" style="width: 100%;" name="clinic_id" id="clinic_id">
                                  @foreach($clinics as $role)
                                  <option value={{ $role->id }}>{{ $role->name }}</option>
                                @endforeach
                                </select>
                            </div>


                            <div class="form-group col-md-4">
                                <label>fees</label>
                                <select class="form-control select2" style="width: 100%;" name="fees_id" id="fees_id">
                                  @foreach($fees as $role)
                                  <option value={{ $role->id }}>{{ $role->name }}</option>
                                @endforeach
                                </select>
                            </div>

                                  <div class="form-group col-md-4">
                                            <label for="image">Image</label>
                                            <input type="file" name="image" class="form-control" id="image"
                                               placeholder="Enter Image">
                                      </div>

                                      <div class="form-group col-md-4">
                                        <label for="cv">Cv</label>
                                        <input type="file" name="cv" class="form-control" id="cv"
                                           placeholder="Enter cv">
                                  </div>

                              </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="button" onclick="performStore()" class="btn btn-primary">Store</button>
                    <a href="{{ route('employee.index') }}" type="button"  class="btn btn-success">Return Back</a>

                </div>
                </form>
            </div>



</section>
@endsection

@section('scripts')
<script src="{{ asset('cms/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('cms/plugins/select2/js/select2.full.min.js') }}"></script>


    <script>
    //        $('.author_id').select2({
    //   theme: 'bootstrap4'
    // })

        function performStore(){

            let formData = new FormData();
                formData.append('city_id',document.getElementById('city_id').value);
                formData.append('fees_id',document.getElementById('fees_id').value);
                formData.append('clinic_id',document.getElementById('clinic_id').value);
                formData.append('name',document.getElementById('name').value);
                formData.append('address',document.getElementById('address').value);
                formData.append('phone',document.getElementById('phone').value);
                formData.append('image',document.getElementById('image').files[0]);
                formData.append('cv',document.getElementById('cv').files[0]);


            store('/cms/employee',formData);
}
</script>
@endsection
