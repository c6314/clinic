@extends('cms.parent')

@section('title' , 'details')

@section('main-title' , 'Index details')

@section('small-title' , 'details')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header ">
          <h3 class="card-title"></h3>
                <a href="{{ route('details.create') }}" type="button"  class="btn btn-success">Add new details</a>

          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" description="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table  table-bordered table-striped table-hover text-nowrap">
            <thead>
              <tr>
                <th>ID</th>
                <th> description</th>
                <th> status</th>
                <th>Created At</th>
                <th>Setting</th>

              </tr>
            </thead>
            <tbody>
                {{-- <td><span class="tag tag-success">Approved</span></td> --}}
                @foreach ($details as $detail )
                <tr>
                    <td>{{ $detail->id }}</td>
                    <td>{{ $detail->description }}</td>
                    <td>{{ $detail->status }}</td>
                    <td>{{ $detail->created_at }}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ route('details.edit', $detail->id) }}" class="btn btn-info">
                              <i class="fas fa-edit"></i>
                            </a>


                        <a href="#" onclick="performDestroy({{ $detail->id }}, this)" class="btn btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </a>

                        </div>
                    </td>
                  </tr>
                @endforeach


            </tbody>
          </table>
        </div>
    </div>
</div>

      </div>
    </div>

    </div>
  </div>
@endsection

@section('scripts')

<script>
    function performDestroy(id , ref){
        confirmDestroy('/cms/details/'+id ,ref);
    }
</script>
@endsection
