@extends('cms.parent')

@section('title' , 'Details')

@section('main-title' , 'Edit Details')

@section('small-title' , 'Details')

@section('styles')

@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="create_form">
                        @csrf
                        <div class="card-body">
                            <div class="row">

                            <div class="form-group col-md-4">
                                <label for="description">description</label>
                                <input type="text" class="form-control" name="description" id="description"
                                    value="{{ $details->description }}" placeholder="Enter your  description">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="status">Status</label>

                                <select class="form-select form-select-sm" name="status" style="width: 100%;"
                                    id="status" aria-label=".form-select-sm example">
                                    <option selected>{{ $details->status }}</option>
                                    <option value="Active">Active</option>
                                    <option value="InActive">InActive</option>
                                </select>
                            </div>


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                </form>
            </div>



</section>
@endsection

@section('scripts')

<script>
    function performUpdate(id){
        let data ={
            description: document.getElementById('description').value,
            status: document.getElementById('status').value,

        }
        let redirectUlr = '/cms/details'
        update('/cms/details/'+ id, data , redirectUlr);

    }

</script>
@endsection
