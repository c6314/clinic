@extends('cms.parent')

@section('title' , 'Details')

@section('main-title' , 'Create Details')

@section('small-title' , 'Details')

@section('styles')
{{-- <link rel="stylesheet" href="{{ asset('cms/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}"> --}}

@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Details</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="create_form">
                        @csrf
                        <div class="card-body">



                            <div class="row">

                            <div class="form-group col-md-4">
                                <label for="description"> description</label>
                                <input type="text" class="form-control" name="description" id="description"
                                    placeholder="Enter your  description">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="status">Status</label>

                                <select class="form-select form-select-sm" name="status" style="width: 100%;"
                                    id="status" aria-label=".form-select-sm example">
                                    <option selected></option>
                                    <option value="Active">Active</option>
                                    <option value="InActive">InActive</option>
                                </select>
                            </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="button" onclick="performStore()" class="btn btn-primary">Store</button>
                    <a href="{{ route('details.index') }}" type="button"  class="btn btn-success">Return Back</a>

                </div>
                </form>
            </div>



</section>
@endsection
{{-- <script src="{{ asset('cms/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script> --}}

@section('scripts')

    <script>
        function performStore(){
            let data ={
                description: document.getElementById('description').value,
                status: document.getElementById('status').value,
            }
            store('/cms/details',data);
        }
    </script>

@endsection
