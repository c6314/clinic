@extends('cms.parent')

@section('title' , 'rooms')

@section('main-title' , 'Create rooms')

@section('small-title' , 'rooms')

@section('styles')
<link rel="stylesheet" href="{{ asset('cms/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('cms/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create rooms</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="create_form">
                        @csrf
                        <div class="card-body">
                           <div class="row">
                            <div class="form-group col-md-4">
                                <label for="room_type">room_type</label>
                                <input type="text" class="form-control" name="room_type" id="room_type"
                                    placeholder="Enter your room type">
                            </div>

                            <div class="form-group col-md-4">
                                <label>reception</label>
                                <select class="form-control select2" style="width: 100%;" name="reception_id" id="reception_id">
                                  @foreach($receptions as $role)
                                  <option value={{ $role->id}}>{{ $role->name }}</option>
                                @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <label>clinic</label>
                                <select class="form-control select2" style="width: 100%;" name="clinic_id" id="clinic_id">
                                  @foreach($clinics as $role)
                                  <option value={{ $role->id }}>{{ $role->name }}</option>
                                @endforeach
                                </select>
                            </div>



                     </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="button" onclick="performStore()" class="btn btn-primary">Store</button>
                    <a href="{{ route('room.index') }}" type="button"  class="btn btn-success">Return Back</a>

                </div>
                </form>
            </div>



</section>
@endsection

@section('scripts')
<script src="{{ asset('cms/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('cms/plugins/select2/js/select2.full.min.js') }}"></script>


    <script>
    //        $('.author_id').select2({
    //   theme: 'bootstrap4'
    // })

        function performStore(){

            let formData = new FormData();
                formData.append('room_type',document.getElementById('room_type').value);
                formData.append('reception_id',document.getElementById('reception_id').value);
                formData.append('clinic_id',document.getElementById('clinic_id').value);

            store('/cms/room',formData);
}
</script>
@endsection
