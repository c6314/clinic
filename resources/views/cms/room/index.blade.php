@extends('cms.parent')

@section('title' , 'room')

@section('main-title' , 'Index room')

@section('small-title' , 'room')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header ">
          <h3 class="card-title"></h3>
                <a href="{{ route('room.create') }}" type="button"  class="btn btn-success">Add new room</a>
          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table  table-bordered table-striped table-hover text-nowrap">
            <thead>
              <tr>
                
                <th>ID</th>
                <th>room type</th>
                <th>reception</th>
                <th>clinic</th>
                <th>Setting</th>
              </tr>
            </thead>
            <tbody>
                {{-- <td><span class="tag tag-success">Approved</span></td> --}}
                @foreach ($rooms as $room )
                <tr>
                    <td>{{ $room->id }}</td>
                    <td>{{ $room->room_type }}</td>
                    <td>{{ $room->reception_id }}</td>
                    <td>{{ $room->clinic_id }}</td>
                    <td>
                        <div class="btn-group">
                            {{-- @can('Edit-room') --}}

                            {{--
                            {{-- @endcan --}}
                            {{-- @can('Delete-room') --}}
                            <a href="{{ route('room.edit', $room->id) }}" class="btn btn-info">
                                <i class="fas fa-edit"></i>
                              </a>

                        <a href="#" onclick="performDestroy({{ $room->id }}, this)" class="btn btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        {{-- @endcan --}}

                    </td>
                  </tr>
                @endforeach


            </tbody>
          </table>
        </div>
    </div>
</div>

        <!-- /.card-body -->
      </div>
    </div>
    {{-- {{ $room->links() }} --}}

      <!-- /.card -->
    </div>
  </div>
@endsection

@section('scripts')

<script>
    function performDestroy(id , ref){
        confirmDestroy('/cms/room/'+id ,ref);
    }
</script>
@endsection
