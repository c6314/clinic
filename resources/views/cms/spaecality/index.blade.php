@extends('cms.parent')

@section('title' , 'Specality')

@section('main-title' , 'Index Specality')

@section('small-title' , ' Specality')

@section('styles')

@endsection

@section('content')
<!-- /.row -->
<div class="row">
  <div class="col-12">
    <div class="card table table-bordered">
      <div class="card-header">
        <h3 class="card-title">index Specality</h3>

        <div class="card-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0" style="height: 300px;">
        <table class="table table-head-fixed text-nowrap">
          <thead>
            <tr>
              <th>ID</th>
              <th>name</th>
              <th>description</th>
              <th>created_at</th>
              <th>updated_at</th>
              <th>setting</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($specality as $specalities )
                <tr>
                  <td>{{ $specalities->id }}</td>
                  <td>{{ $specalities->name }}</td>
                  <td>{{ $specalities->description}}</td>
                  <td>{{ $specalities->created_at }}</td>
                  <td>{{ $specalities->updated_at}}</td>
                  <td>
                    <div class="btn-group">
                        <a href="{{ route('spec.edit',$specalities->id)}}" class="btn btn-info">
                          <i class="fas fa-edit"></i>
                        </a>
                        <form action="{{ route('spec.destroy', $specalities->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                           <button type="submit" class="btn btn-danger">
                             <i class="fas fa-trash-alt"></i>
                           </button>
                        </form>

                   </div>
                  </td>
            </tr>
              @endforeach

          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>
<!-- /.row -->

@endsection

@section('scripts')

@endsection
