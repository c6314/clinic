@extends('cms.parent')

@section('title' , 'Specality')

@section('main-title' , 'Create Specality')

@section('small-title' , 'Specality')

@section('styles')
<link rel="stylesheet" href="{{ asset('cms/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')
<div class="row">

    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Quick Example</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route('spec.store')}}" method="POST">
            @csrf
          <div class="card-body">
            <div class="col-md-9">
                <div class="form-group">
                  <label>clinic</label>
                  <select class="form-control select2" name="clinic_id" style="width: 100%;" id="clinic_id">
                    @foreach ($clinic as $clinics)
                    <option value="{{ $clinics->id }}">{{ $clinics->name}}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-group">
              <label for="name">name</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
            </div>
            <div class="form-group">
              <label for="des">description</label>
              <input type="text" class="form-control" id="des" name="description" placeholder="Enter description">
            </div>

          <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-info">Store</button>
             </div>


          <!-- /.card-footer -->
        </form>
      </div>
      <!-- /.card -->
@endsection

@section('scripts')
<script src="{{ asset('cms/plugins/select2/js/select2.full.min.js') }}"></script>
@endsection
