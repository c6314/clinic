@extends('cms.parent')

@section('title' , 'Specality')

@section('main-title' , 'Edit Specality')

@section('small-title' , 'Specality')

@section('styles')

@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Quick Example</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route('spec.store')}}" method="POST">
            @csrf
          <div class="card-body">
            <div class="col-md-9">
                <div class="form-group">
                  <label>clinic</label>
                  <select class="form-control select2" name="clinic_id" style="width: 100%;" id="clinic_id">
                    @foreach ($clinic as $clinics)
                    <option value="{{ $clinics->id }}">{{ $clinics->name}}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-group">
              <label for="name">name</label>
              <input type="text" class="form-control" id="name" name="name" value="{{ $specality->name }}" placeholder="Enter name">
            </div>
            <div class="form-group">
              <label for="des">description</label>
              <input type="text" class="form-control" id="des" name="description" value="{{ $specality->description }}" placeholder="Enter description">
            </div>

          <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-info">Update</button>
             </div>


          <!-- /.card-footer -->
        </form>
      </div>
      <!-- /.card -->
@endsection

@section('scripts')

@endsection
