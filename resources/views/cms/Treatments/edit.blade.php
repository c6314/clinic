@extends('cms.parent')

@section('title' , 'Treatment')

@section('main-title' , 'Edit Treatment')

@section('small-title' , 'Treatments')

@section('styles')

@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Treatment</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="create_form">
                        @csrf
                        <div class="card-body">
                            <div class="row">

                            <div class="form-group col-md-4">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name"
                                    value="{{ $treatments->name }}" placeholder="Enter your  name">
                            </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                </form>
            </div>



</section>
@endsection

@section('scripts')

<script>
    function performUpdate(id){
        let data ={
            name: document.getElementById('name').value,

        }
        let redirectUlr = '/cms/treatments'
        update('/cms/treatments/'+ id, data , redirectUlr);

    }

</script>
@endsection
