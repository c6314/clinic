@extends('cms.parent')

@section('title' , 'Treatment')

@section('main-title' , 'Create Treatment')

@section('small-title' , 'Treatments')

@section('styles')
<link rel="stylesheet" href="{{ asset('cms/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">

@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Treatment</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="create_form">
                        @csrf
                        <div class="card-body">



                            <div class="row">

                            <div class="form-group col-md-4">
                                <label for="name"> Name</label>
                                <input type="text" class="form-control" name="name" id="name"
                                    placeholder="Enter your  name">


                                    <div class="form-group">
                                        <label>Doctor</label>
                                        <select class="form-control select2" name="doctor_id" style="width: 100%;" id="doctor_id">
                                          @foreach ($doctors as $doctor)
                                          <option value="{{ $doctor->id }}">{{ $doctor->name }}</option>
                                          @endforeach
                                        </select>
                                      </div>



                                      <div class="form-group">
                                        <label>Patient</label>
                                        <select class="form-control select2" name="patient_id" style="width: 100%;" id="patient_id">
                                          @foreach ($patients as $patient)
                                          <option value="{{ $patient->id }}">{{ $patient->name }}</option>
                                          @endforeach
                                        </select>
                                      </div>

                            </div>

                                 
                            </div>



                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="button" onclick="performStore()" class="btn btn-primary">Store</button>
                    <a href="{{ route('treatments.index') }}" type="button"  class="btn btn-success">Return Back</a>

                </div>
                </form>
            </div>



</section>
@endsection
{{-- <script src="{{ asset('cms/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script> --}}

@section('scripts')

    <script>
        function performStore(){
            let data ={
                name: document.getElementById('name').value,

            }
            store('/cms/treatments',data);
        }
    </script>

@endsection
