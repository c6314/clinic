@extends('cms.parent')

@section('title' , 'Recepion')

@section('main-title' , 'Edit Recepion')

@section('small-title' , 'Recepion')

@section('styles')

@endsection

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Quick Example</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route('recepion.store')}}" method="POST">
            @csrf
          <div class="card-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                      <label>fees</label>
                      <select class="form-control select2" name="fees_id" style="width: 100%;" id="fees_id">
                        @foreach ($fees as $fee)
                        <option value="{{ $fee->id }}">{{ $fee->fees}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" value={{ $recepion->name }} placeholder="Enter name">
            </div>
            <div class="form-group">
              <label for="phone">Phone</label>
              <input type="text" class="form-control" id="phone" name="phone" value={{ $recepion->phone }} placeholder="Enter phone">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" value={{ $recepion->email }} placeholder="Enter email">
              </div>
              <div class="form-group">
                <label for="booking">Date_Of_Booking</label>
                <input type="text" class="form-control" id="booking" name="date_of_booking" value={{ $recepion->date_of_booking }} placeholder="Enter date_of_booking">
              </div>


          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
      <!-- /.card -->


@endsection

@section('scripts')

@endsection
