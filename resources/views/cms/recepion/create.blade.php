@extends('cms.parent')

@section('title' , 'Recepion')

@section('main-title' , 'Create Recepion')

@section('small-title' , 'Recepion')

@section('styles')
<link rel="stylesheet" href="{{ asset('cms/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Create Recepion</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route('recepion.store')}}" method="POST">
            @csrf
          <div class="card-body">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                      <label>fees</label>
                      <select class="form-control select2" name="fees_id" style="width: 100%;" id="fees_id">
                        @foreach ($fees as $fee)
                        <option value="{{ $fee->id }}">{{ $fee->fees}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
            </div>
            <div class="form-group">
              <label for="phone">Phone</label>
              <input type="text" class="form-control" name="phone" id="phone" placeholder="Enter phone">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="Enter email">
              </div>
              <div class="form-group ">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" id="password"
                    placeholder="Enter your password ">
            </div>
              <div class="form-group">
                <label for="date">Date_Of_Booking</label>
                <input id="date" type="date"  name="date_of_booking" class="form-control"
                    placeholder="date of booking">
            </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Store</button>
            <a href="{{ route('recepion.index')}}" type="button"  class="btn btn-primary">Return Back</a>

        </div>
        </form>
      </div>
      <!-- /.card -->


@endsection

@section('scripts')
<script src="{{ asset('cms/plugins/select2/js/select2.full.min.js') }}"></script>

@endsection
