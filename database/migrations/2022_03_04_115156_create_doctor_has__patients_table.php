<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorHasPatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_has__patients', function (Blueprint $table) {
            $table->id();

            $table->foreignId('doctors_id');
            $table->foreign('doctors_id')->references('id')->on('doctors');

            $table->foreignId('patients_id');
            $table->foreign('patients_id')->references('id')->on('patients');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_has__patients');
    }
}
