<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('phone');
            $table->string('address');
            $table->string('image');
            $table->string('cv');
           
            $table->foreignId('clinic_id');
            $table->foreign('clinic_id')->references('id')->on('clinics');

            $table->foreignId('city_id');
            $table->foreign('city_id')->references('id')->on('cities');

            $table->foreignId('fees_id');
            $table->foreign('fees_id')->references('id')->on('fees');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
