<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('age');
            $table->string('phone');

            $table->string('address');
            $table->string('gender');
            $table->foreignId('doctor_id');
            $table->foreign('doctor_id')->references('id')->on('doctors');

            $table->foreignId('room_id');
            $table->foreign('room_id')->references('id')->on('rooms');

            $table->foreignId('clinic_id');
            $table->foreign('clinic_id')->references('id')->on('clinics');


            $table->foreignId('reception_id');
            $table->foreign('reception_id')->references('id')->on('receptions');

            $table->foreignId('fees_id');
            $table->foreign('fees_id')->references('id')->on('fees');



            $table->foreignId('city_id');
            $table->foreign('city_id')->references('id')->on('cities');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
