<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsHasSpecalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients_has_specalities', function (Blueprint $table) {
            $table->id();
            $table->foreignId('specalities_id');
            $table->foreign('specalities_id')->references('id')->on('specalities');

            $table->foreignId('patients_id');
            $table->foreign('patients_id')->references('id')->on('patients');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients_has_specalities');
    }
}
