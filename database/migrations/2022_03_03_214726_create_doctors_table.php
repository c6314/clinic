<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('phone');
            $table->string('address');
            $table->string('email');
            $table->string('salary');
            $table->string('cv');
            $table->string('image');

            $table->foreignId('specalities_id');
            $table->foreign('specalities_id')->references('id')->on('specalities');

            $table->foreignId('clinic_id');
            $table->foreign('clinic_id')->references('id')->on('clinics');

            $table->foreignId('room_id');
            $table->foreign('room_id')->references('id')->on('rooms');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
