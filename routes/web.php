<?php

use App\Http\Controllers\CityController;
use App\Http\Controllers\ClinicController;
use App\Http\Controllers\DetailsController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\EquipmentsController;
use App\Http\Controllers\FeesController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PatientsController;
use App\Http\Controllers\RecepionAuthController;
use App\Http\Controllers\ReceptionController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\roomsController;
use App\Http\Controllers\SpecalitiesController;
use App\Http\Controllers\TreatmentsController;
use App\Http\Controllers\UserAuthController;
// use App\Http\Controllers\roomsController;
// use App\Http\Controllers\HomeController;


use App\Models\employee;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('cms.parent');
// });

Route::prefix('cms/')->middleware('guest:reception')->group(function(){
    Route::get('{guard}/login' , [UserAuthController::class , 'showLogin'])->name('login.view');
    Route::post('{guard}/login' , [UserAuthController::class , 'login']);
});

// Route::prefix('cms')->middleware('auth:admin')->group(function(){
//     Route::get('/logout' , [UserAuthController::class , 'logout'])->name('cms.admin.logout');
// });

// middleware('auth:reception')->
Route::prefix('cms')->group(function(){
    Route::resource('doctor' , DoctorController::class);
    // Route::post('/update-doctor/{id}' , DoctorController::class ,'update')->name('update-doctor');
    Route::post('/update-doctor/{id}' , [DoctorController::class  , 'update'])->name('update-doctor');

    // Route::post('/update-doctor/{id}', 'DoctorController@update')->name('update-doctor');
    Route::resource('patient' , PatientsController::class);
    Route::resource('employee' , EmployeeController::class);
    Route::post('/update-employee/{id}' , [EmployeeController::class  , 'update'])->name('update-employee');

    Route::resource('room',RoomController::class);
    Route::resource('fees' ,FeesController::class);
    Route::resource('clinics' ,ClinicController::class);
    Route::resource('cities' ,CityController::class);
    Route::resource('recepion' ,ReceptionController::class);
    Route::resource('details' ,DetailsController::class);
    Route::resource('treatments' ,TreatmentsController::class);
    Route::resource('spec' ,SpecalitiesController::class);
    Route::resource('equ' ,EquipmentsController::class);

});

Route::prefix('website')->group(function(){

Route::resource('home',HomeController::class);
});
